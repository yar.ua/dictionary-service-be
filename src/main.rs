mod db_fixture;
mod datatypes;
mod rocket_cors;
mod web_service;
mod app_config;
mod session;
mod errors;
mod database;

#[macro_use]
extern crate rocket;

use std::path::Path;
use rocket::fairing::AdHoc;
use rocket_oauth2::OAuth2;
use crate::db_fixture::Database;
use web_service::*;
use crate::database::db_git;
use crate::database::snapshot::Snapshot;
use crate::datatypes::{NewGrammeme, NewParadigm};

#[launch]
fn rocket() -> _ {
    let a = Database::new();
    let root = Path::new("tmp");
    let mut bc = db_git::Database::new();
    let result = bc.add_paradigms(Snapshot::BranchHead { branch: "main".to_string() }, vec![NewParadigm {
        text: "жо́втий".to_string(),
        pos: "A".to_string(),
        categories: "qaul".to_string(),
        grammemes: vec![
            NewGrammeme { text: "жо́втий".to_string(), categories: "sg;m;nom".to_string() },
            NewGrammeme { text: "жо́втого".to_string(), categories: "sg;m;gen".to_string() },
            NewGrammeme { text: "жо́втому".to_string(), categories: "sg;m;dat".to_string() },
            NewGrammeme { text: "жо́втого".to_string(), categories: "sg;m;acc".to_string() },
            NewGrammeme { text: "жо́втому".to_string(), categories: "sg;m;loc".to_string() },
            NewGrammeme { text: "жо́втім".to_string(), categories: "sg;m;loc".to_string() },
            NewGrammeme { text: "жо́втим".to_string(), categories: "sg;m;inst".to_string() },
            NewGrammeme { text: "жо́втий".to_string(), categories: "sg;m;voc".to_string() },
        ],
    }]);
    if let Snapshot::Branch { revision, branch } = result.unwrap().1 {
        bc.save(root, revision);
    }

    rocket::custom(app_config::app_figment())
        .manage(Database::new())
        .mount("/api", routes![
                user,
                get_paradigm,
                find_paradigms,
                patch_paradigm,
                opt_paradigm,
                google_login,
                google_callback,
                logout,
            ], )
        .attach(AdHoc::config::<app_config::Config>())
        .attach(OAuth2::<Google>::fairing("google"))
        .attach(rocket_cors::Cors)
}
