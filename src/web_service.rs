use crate::db_fixture::Database;
use crate::datatypes::{Paradigm, UpdatedParadigm, User};
use serde::{Deserialize};
use rocket::State;
use std::ops::Deref;
use rocket::http::CookieJar;
use rocket::response::Redirect;
use crate::app_config;
use rocket_oauth2::{OAuth2, TokenResponse};
use reqwest;
use rocket::serde::json::Json;
use crate::session::{Session, create_session_cookie, SESSION_COOKIE_KEY};
use tracing::{info, error};

pub struct Google;

#[get("/login/google")]
pub fn google_login(config: &State<app_config::Config>, oauth2: OAuth2<Google>, cookies: &CookieJar<'_>) -> Redirect {
    info!("attempting a login using google");
    oauth2.get_redirect_extras(
        cookies,
        &[
            "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/userinfo.email"
        ],
        &[("redirect_uri", &config.inner().oauth.google.redirect_uri)]).unwrap()
}

#[get("/auth/google")]
pub async fn google_callback(config: &State<app_config::Config>, code: TokenResponse<Google>, cookies: &CookieJar<'_>) -> Redirect {
    let access_token = code.access_token().to_string();

    async fn get_user_info(access_token: String) -> Result<User, reqwest::Error> {
        #[derive(Deserialize)]
        struct UserInfo {
            id: String,
            email: String,
            verified_email: bool,
            name: String,
            given_name: String,
            family_name: String,
            picture: String,
            locale: String,
        }

        let response = reqwest::Client::builder().build()?
            .get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json")
            .bearer_auth(access_token).send().await?;
        let user_info = response.json::<UserInfo>().await?;

        Ok(User {
            id: user_info.id,
            name: user_info.name,
            email: user_info.email,
        })
    }

    let user = match get_user_info(access_token).await {
        Ok(user) => user,
        Err(error) => {
            error!("get_user_info error {}", error);
            return Redirect::to("/");
        }
    };

    let cookie = create_session_cookie(user, config.inner().oauth.secure);
    cookies.add_private(cookie);
    info!("successfully created session");
    Redirect::to("/")
}

#[post("/logout")]
pub fn logout(cookies: &CookieJar<'_>, session: Session) -> Redirect {
    cookies.remove_private(SESSION_COOKIE_KEY);
    Redirect::to("/")
}

#[get("/user")]
pub fn user(session: Session) -> Option<Json<User>> {
    Some(Json(session.user))
}

#[get("/paradigm/<id>")]
pub fn get_paradigm(state: &State<Database>, id: String) -> Option<Json<Paradigm>> {
    match state.inner().get_paradigm(&id) {
        Some(paradigm) => Some(Json(paradigm)),
        _ => None,
    }
}

#[patch("/paradigm", data = "<paradigm>")]
pub fn patch_paradigm(
    state: &State<Database>,
    paradigm: Json<UpdatedParadigm>,
    session: Session,
) -> Option<Json<Paradigm>> {
    let mut lock = state.lock();

    match (lock.update_paradigm(paradigm.deref())) {
        Some(paradigm) => Some(Json(paradigm)),
        _ => None,
    }
}

#[get("/paradigms?<pos>&<text>&<limit>&<offset>")]
pub fn find_paradigms(
    state: &State<Database>,
    pos: Option<&'_ str>,
    text: Option<&'_ str>,
    limit: Option<i32>,
    offset: Option<i32>,
) -> Option<Json<Vec<Paradigm>>> {
    return Some(Json(state.inner().find_paradigms(
        text.map(String::from),
        pos.map(String::from),
        limit,
        offset,
    )));
}

#[options("/paradigm")]
pub fn opt_paradigm() -> &'static str {
    ""
}
