#[derive(Debug)]
pub enum Error {
    Unauthorized,
    BadCookie,
    NotImplemented,
}
