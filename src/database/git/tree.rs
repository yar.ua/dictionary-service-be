use std::collections::BTreeMap;
use std::io;
use std::io::{BufRead, Write};
use crate::database::git::deferred_object::DeferredGitObject;
use crate::database::git::object::SPACE;
use crate::database::git::errors::Error;
use crate::database::git::object_id::{ObjectId};
use crate::database::git::object_store::ObjectStore;

#[derive(PartialEq, Debug, Clone)]
pub struct GitTree {
    pub nodes: BTreeMap<String, GitTreeNode>,
}

impl GitTree {
    pub fn new() -> Self {
        return Self { nodes: BTreeMap::new() };
    }

    pub fn read(reader: &mut dyn BufRead, size: usize) -> Result<Self, Error> {
        let mut bytes_read = 0;
        let mut nodes = BTreeMap::new();
        while bytes_read < size {
            let node = GitTreeNode::read(reader).or_else(Error::io)?;
            bytes_read += node.size();
            nodes.insert(node.name.clone(), node);
        }
        return Ok(GitTree {
            nodes,
        });
    }

    pub fn write(&self, writer: &mut dyn Write) -> Result<(), io::Error> {
        for (_, node) in &self.nodes {
            node.write(writer)?;
        }
        return Ok(());
    }

    pub fn size(&self) -> usize {
        return self.nodes.values().map(|x| x.size()).sum::<usize>() + self.nodes.len() - 1;
    }

    pub fn store_unchecked(&mut self, object_store: &mut ObjectStore) {
        for (_, node) in &mut self.nodes {
            node.store_unchecked(object_store);
        }
    }
}

// MARK: GitTreeNode
#[derive(PartialEq, Debug, Clone)]
pub struct GitTreeNode {
    pub mode: GitTreeNodeMode,
    pub name: String,
    pub object: DeferredGitObject,
}

impl GitTreeNode {
    fn read(reader: &mut dyn BufRead) -> Result<Self, io::Error> {
        let mode = GitTreeNodeMode::read(reader)?;
        let mut name_buf = Vec::new();
        reader.read_until(0, &mut name_buf)?;
        name_buf.pop();
        let name = String::from_utf8(name_buf).unwrap();
        let object_id = ObjectId::read_bytes(reader)?;
        return Ok(Self {
            mode,
            name,
            object: DeferredGitObject::NotLoaded(object_id),
        });
    }

    fn write(&self, writer: &mut dyn Write) -> Result<(), io::Error> {
        writer.write_all(self.mode.ascii())?;
        writer.write_all(&[SPACE])?;
        write!(writer, "{}", self.name)?;
        writer.write_all(&[0])?;
        self.object.get_id().write_bytes(writer)?;
        return Ok(());
    }

    fn store_unchecked(&mut self, object_store: &mut ObjectStore) -> &ObjectId {
        return self.object.store_unchecked(object_store);
    }

    fn size(&self) -> usize {
        return self.mode.size() + 1 + self.name.len() + 1 + ObjectId::size_bytes();
    }
}

// MARK: GitTreeNodeMode
#[derive(Clone, Copy, PartialEq, Eq, Debug, Ord, PartialOrd, Hash)]
#[repr(u16)]
pub enum GitTreeNodeMode {
    /// A tree, or directory
    Tree = 0o040000u16,
    /// A file that is not executable
    Blob = 0o100644,
    /// A file that is executable
    BlobExecutable = 0o100755,
    /// A symbolic link
    Link = 0o120000,
    /// A commit of a git submodule
    Commit = 0o160000,
}

impl GitTreeNodeMode {
    fn read(reader: &mut dyn BufRead) -> Result<Self, io::Error> {
        let mut buf = Vec::with_capacity(6);
        reader.read_until(SPACE, &mut buf)?;
        buf.pop();
        return match buf.as_slice() {
            b"40000" => Ok(Self::Tree),
            b"100644" => Ok(Self::Blob),
            b"100755" => Ok(Self::BlobExecutable),
            b"120000" => Ok(Self::Link),
            b"160000" => Ok(Self::Commit),
            _ => Err(io::Error::from(io::ErrorKind::InvalidData)),
        };
    }

    fn ascii(&self) -> &'static [u8] {
        return match self {
            GitTreeNodeMode::Tree => b"40000",
            GitTreeNodeMode::Blob => b"100644",
            GitTreeNodeMode::BlobExecutable => b"100755",
            GitTreeNodeMode::Link => b"120000",
            GitTreeNodeMode::Commit => b"160000",
        };
    }

    fn write(&self, writer: &mut dyn Write) -> Result<(), io::Error> {
        return writer.write_all(self.ascii());
    }
    fn size(&self) -> usize {
        return match self {
            GitTreeNodeMode::Tree => 5,
            GitTreeNodeMode::Blob => 6,
            GitTreeNodeMode::BlobExecutable => 6,
            GitTreeNodeMode::Link => 6,
            GitTreeNodeMode::Commit => 6,
        };
    }
}

// MARK: Tests
#[cfg(test)]
mod tests {
    use std::io;
    use crate::database::git::deferred_object::DeferredGitObject;
    use crate::database::git::object_id::ObjectId;
    use super::{GitTreeNode, GitTreeNodeMode};

    #[test]
    fn test_read_tree_node() {
        let bytes = b"100644 .gitignore\0\x80\x91\xc5\xa4\xe1\xa4\x8c\xcc\x03\xc0\x01\x6b\x0d\xe8\xa1\x8c\xd6\x24\xd4\x30";
        let mut read = io::Cursor::new(bytes);
        let node = GitTreeNode {
            mode: GitTreeNodeMode::Blob,
            name: ".gitignore".to_string(),
            object: DeferredGitObject::NotLoaded(ObjectId::from([0x80u8, 0x91u8, 0xc5u8, 0xa4u8, 0xe1u8, 0xa4u8, 0x8cu8, 0xccu8, 0x03u8, 0xc0u8, 0x01u8, 0x6bu8, 0x0du8, 0xe8u8, 0xa1u8, 0x8cu8, 0xd6u8, 0x24u8, 0xd4u8, 0x30u8])),
        };
        assert_eq!(node, GitTreeNode::read(&mut read).unwrap());
        let mut out_bytes = Vec::new();
        node.write(&mut out_bytes).unwrap();
        assert_eq!(bytes, out_bytes.as_slice());
    }
}
