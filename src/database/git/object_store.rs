use std::collections::HashMap;
use std::fs::File;
use std::{fs, io};
use std::io::{Cursor, Write};
use std::path::Path;
use crate::database::snapshot::Snapshot;
use crate::database::git::errors::Error;
use crate::database::git::object::GitObject;
use crate::database::git::object_id::ObjectId;
use crate::database::git::tree::GitTree;
use flate2::Compression;
use flate2::write::ZlibEncoder;
use matches::debug_assert_matches;

#[derive(Debug)]
pub struct ObjectStore {
    storage: HashMap<ObjectId, GitObject>,
}

impl ObjectStore {
    pub fn new() -> Self {
        return Self {
            storage: HashMap::new(),
        };
    }

    pub fn set_object_unchecked(&mut self, object: GitObject) -> ObjectId {
        // Maybe this should check if the object exists, then check for collisions which is extremely unlikely
        let id = object.get_id();
        self.storage.insert(id.clone(), object);
        return id;
    }

    pub fn get_object<'a>(&'a self, at: &'a ObjectId) -> Result<&'a GitObject, Error> {
        let object = self.storage.get(at);
        if let None = object {
            return Err(Error::ObjectNotFound(at.clone()));
        }
        return Ok(object.unwrap());
    }

    pub fn get_object_unchecked<'a>(&'a self, at: &'a ObjectId) -> &'a GitObject {
        let object = self.storage.get(at);
        debug_assert_matches!(object, Some(_));
        return object.unwrap();
    }

    pub async fn commit_tree(&mut self, head: &Snapshot, tree: GitTree) -> Result<Snapshot, Error> {
        todo!()
    }
}

impl ObjectStore {
    // TODO: Temporary

    pub fn save(&self, root: &Path, revision: ObjectId) {
        let objects_path = root.join("objects");
        fs::create_dir_all(&objects_path).unwrap();
        for (k, v) in &self.storage {
            let mut hash = Cursor::new(Vec::new());
            k.write_ascii(&mut hash).unwrap();
            let ascii_hash = String::from_utf8(hash.into_inner()).unwrap();
            let (dir, file) = ascii_hash.split_at(2);
            let dir_path = objects_path.join(dir);
            fs::create_dir_all(&dir_path).unwrap();

            let writer = File::create(dir_path.join(file)).unwrap();
            let mut writer = ZlibEncoder::new(writer, Compression::default());

            match v.write(&mut writer) {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Failed to save object {}", e);
                }
            };
            writer.flush().unwrap();
        }

        let refs_path = root.join("refs/heads");
        fs::create_dir_all(&refs_path).unwrap();
        let mut writer = File::create(refs_path.join("main")).unwrap();
        let _ = revision.write_ascii(&mut writer);
    }
}