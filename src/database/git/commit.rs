use std::io;
use std::time::Duration;
use time::{OffsetDateTime, UtcOffset};
use crate::database::git::object::{LINE_FEED, SPACE};
use crate::database::git::errors::Error;
use crate::database::git::object_id::{ObjectId, OBJECT_ID_LEN};

#[derive(PartialEq, Debug, Clone)]
pub struct GitCommit {
    pub tree: ObjectId,
    pub parents: Vec<ObjectId>,
    pub author: Person,
    pub committer: Person,
    pub message: String,
}

/*
Commit Structure:
[b"commit", SPACE, size_of_content_bytes_as_ascii, 0, tree, [...parents], author, committer, LINE_FEED, commit_message, LINE_FEED]
tree = [b"tree", SPACE, tree_hash_hex_as_ascii, LINE_FEED]
parent = [b"parent", SPACE, parent_commit_hash_hex_as_ascii, LINE_FEED]
author = [b"author", SPACE, author_name, SPACE, b'<', author_email, b'>', SPACE, unix_time_stamp, SPACE, time_zone, LINE_FEED]
committer = [b"committer", SPACE, committer_name, SPACE, b'<', committer_email, b'>', SPACE, unix_time_stamp, SPACE, time_zone, LINE_FEED]
 */

impl GitCommit {
    pub fn read(reader: &mut dyn io::BufRead, size: usize) -> Result<Self, Error> {
        let mut kind_buff = Vec::new();
        reader.read_until(SPACE, &mut kind_buff).or_else(Error::io)?;
        kind_buff.pop(); // pop trailing space

        if kind_buff.as_slice() != b"tree" {
            return Err(Error::InvalidObjectFormat("commit object missing tree section"));
        }

        let tree = ObjectId::read_ascii(reader).or_else(Error::io)?;
        reader.consume(1); // line feed

        let mut parents = Vec::new();
        loop {
            kind_buff = Vec::new();
            reader.read_until(SPACE, &mut kind_buff).or_else(Error::io)?;
            kind_buff.pop(); // pop trailing space
            if kind_buff.as_slice() != b"parent" {
                break;
            }
            let parent_id = ObjectId::read_ascii(reader).or_else(Error::io)?;
            parents.push(parent_id);
            reader.consume(1); // line feed
        }

        if kind_buff.as_slice() != b"author" {
            return Err(Error::InvalidObjectFormat("commit object missing author section"));
        }
        let author = Person::read(reader).or_else(Error::io)?;
        reader.consume(1); // line feed

        kind_buff = Vec::new();
        reader.read_until(SPACE, &mut kind_buff).or_else(Error::io)?;
        kind_buff.pop(); // pop trailing space
        if kind_buff.as_slice() != b"committer" {
            return Err(Error::InvalidObjectFormat("commit object missing committer section"));
        }
        let committer = Person::read(reader).or_else(Error::io)?;
        reader.consume(1); // line feed

        reader.consume(1); // line feed
        let mut message = String::new();
        reader.read_to_string(&mut message).or_else(Error::io)?;
        message.pop(); // trailing line feed

        return Ok(GitCommit {
            tree,
            parents,
            author,
            committer,
            message,
        });
    }

    pub fn write(&self, writer: &mut dyn io::Write) -> Result<(), io::Error> {
        write!(writer, "tree ")?;
        self.tree.write_ascii(writer)?;
        writer.write_all(&[LINE_FEED])?;
        for parent in &self.parents {
            write!(writer, "parent ")?;
            parent.write_ascii(writer)?;
            writer.write_all(&[LINE_FEED])?;
        }
        write!(writer, "author ")?;
        self.author.write(writer)?;
        writer.write_all(&[LINE_FEED])?;
        write!(writer, "committer ")?;
        self.committer.write(writer)?;
        writer.write_all(&[LINE_FEED, LINE_FEED])?;
        write!(writer, "{}", self.message)?;
        writer.write_all(&[LINE_FEED])?;
        return Ok(());
    }

    pub fn size(&self) -> usize {
        return 5 + OBJECT_ID_LEN * 2 + (8 + OBJECT_ID_LEN * 2) * self.parents.len() + 7 + self.author.size() + 11 + self.committer.size() + 2 + self.message.len() + 2;
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Person {
    pub name: String,
    pub email: String,
    pub date: OffsetDateTime,
}
// author = [b"author", SPACE, author_name, SPACE, b'<', author_email, b'>', SPACE, unix_time_stamp, SPACE, time_zone, LINE_FEED]
// committer = [b"committer", SPACE, committer_name, SPACE, b'<', committer_email, b'>', SPACE, unix_time_stamp, SPACE, time_zone, LINE_FEED]

impl Person {
    pub fn read(reader: &mut dyn io::BufRead) -> Result<Self, io::Error> {
        let mut buffer = Vec::new();
        reader.read_until(SPACE, &mut buffer)?;
        buffer.pop(); // trailing space
        let name = String::from_utf8(buffer).unwrap();

        buffer = Vec::new();
        reader.consume(1); // b'<'
        reader.read_until(b'>', &mut buffer)?;
        buffer.pop(); // b'>'
        let email = String::from_utf8(buffer).unwrap();

        let mut date_buffer = [0u8; 10];
        reader.read_exact(&mut date_buffer)?;
        let secs = u64::from_str_radix(std::str::from_utf8(&date_buffer).unwrap(), 10).unwrap();
        let date = OffsetDateTime::UNIX_EPOCH + Duration::from_secs(secs);
        reader.consume(1); // space
        let mut time_offset_hours_buffer = [0u8; 3];
        reader.read_exact(&mut time_offset_hours_buffer)?;
        let offset_hours = i8::from_str_radix(std::str::from_utf8(&time_offset_hours_buffer).unwrap(), 10).unwrap();

        let mut time_offset_minutes_buffer = [0u8; 2];
        reader.read_exact(&mut time_offset_minutes_buffer)?;
        let offset_minutes = i8::from_str_radix(std::str::from_utf8(&time_offset_minutes_buffer).unwrap(), 10).unwrap();
        let offset = UtcOffset::from_hms(offset_hours, offset_minutes, 0).unwrap();
        let date = date.replace_offset(offset);

        return Ok(Person { name, email, date });
    }

    fn write(&self, write: &mut dyn io::Write) -> Result<(), io::Error> {
        write!(write, "{} <{}> ", self.name, self.email)?;
        write!(write, "1716019359 +0200")?; // TODO: Write date
        return Ok(());
    }

    fn size(&self) -> usize {
        return self.name.len() + 2 + self.email.len() + 2 + 10 + 1 + 5;
    }
}