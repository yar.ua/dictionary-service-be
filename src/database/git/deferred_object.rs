use crate::database::git::errors::Error;
use crate::database::git::errors::Error::ObjectNotFound;
use crate::database::git::object::GitObject;
use crate::database::git::object_id::ObjectId;
use crate::database::git::object_store::ObjectStore;

#[derive(PartialEq, Debug, Clone)]
pub enum DeferredGitObject {
    Loaded(GitObject),
    NotLoaded(ObjectId),
}

impl DeferredGitObject {
    pub fn get_id(&self) -> ObjectId {
        return match self {
            DeferredGitObject::Loaded(x) => x.get_id(),
            DeferredGitObject::NotLoaded(x) => x.clone(),
        };
    }

    pub fn store_unchecked(&mut self, object_store: &mut ObjectStore) -> &ObjectId {
        if let DeferredGitObject::Loaded(object) = self {
            *self = Self::NotLoaded(object.store_unchecked(object_store))
        }
        if let Self::NotLoaded(id) = self {
            return id;
        };
        unreachable!()
    }

    pub fn load(&mut self, object_store: &ObjectStore) -> Result<&mut GitObject, Error> {
        if let Self::NotLoaded(object_id) = self {
            let object = object_store.get_object(object_id)?;
            *self = Self::Loaded(object.clone());
        }
        if let Self::Loaded(object) = self {
            return Ok(object);
        }
        unreachable!()
    }

    pub fn load_unchecked(&mut self, object_store: &ObjectStore) -> &mut GitObject {
        return self.load_unchecked(object_store);
    }
    
    pub fn loaded<'a>(&'a self, object_store: &'a ObjectStore) -> Result<&'a GitObject, Error> {
        return match self {
            Self::NotLoaded(object_id) => {
                return match object_store.get_object(object_id) {
                    Ok(object) => Ok(object),
                    _ => Err(ObjectNotFound(object_id.clone()))
                };
            }
            Self::Loaded(object) => Ok(object),
        };
    }
}
