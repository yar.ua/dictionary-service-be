use std::io;

#[derive(PartialEq, Debug, Clone)]
pub struct GitTag {}

impl GitTag {
    pub fn read(reader: &mut dyn io::BufRead, size: usize) -> Result<Self, io::Error> {
        unimplemented!()
    }
    pub fn write(&self, writer: &mut dyn io::Write) -> Result<(), io::Error> {
        unimplemented!()
    }

    pub fn size(&self) -> usize {
        unimplemented!()
    }
}