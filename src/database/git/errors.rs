use std::fmt::{Display, Formatter};
use std::io;
use crate::database::git::object_id::ObjectId;

#[derive(Debug)]
pub enum Error {
    ObjectNotFound(ObjectId),
    ObjectTypeNotRecognized(String),
    IO(io::Error),
    NotImplemented,
    InvalidObjectFormat(&'static str),
    ObjectKindMismatch(&'static str),
    ChildNotFound(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        return write!(f, "error");
    }
}

impl Error {
    pub fn io<T>(err: io::Error) -> Result<T, Self> {
        return Err(Self::IO(err));
    }
}
