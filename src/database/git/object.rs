use std::io;
use crate::database::git::commit::GitCommit;
use crate::database::git::errors::Error;
use crate::database::git::object_id::ObjectId;
use crate::database::git::object_store::ObjectStore;
use crate::database::git::tag::GitTag;
use crate::database::git::tree::GitTree;

pub const SPACE: u8 = b' ';
pub const LINE_FEED: u8 = b'\n';

#[derive(PartialEq, Debug, Clone)]
pub enum GitObject {
    Tree(GitTree),
    Blob(Vec<u8>),
    Commit(GitCommit),
    Tag(GitTag),
}

impl GitObject {
    pub fn get_id(&self) -> ObjectId {
        return ObjectId::from_object(self);
    }

    pub fn from_bytes(reader: &mut dyn io::BufRead) -> Result<Self, Error> {
        let mut kind_buff = Vec::new();
        reader.read_until(0, &mut kind_buff).or_else(Error::io)?;
        kind_buff.pop();
        let mut size_buff = Vec::new();
        reader.read_until(SPACE, &mut size_buff).or_else(Error::io)?;
        size_buff.pop();
        let size = String::from_utf8(size_buff).unwrap().parse::<usize>().unwrap();
        return match kind_buff.as_slice() {
            b"tree" => {
                Ok(Self::Tree(GitTree::read(reader, size)?))
            }
            b"blob" => {
                let mut buf = vec![0u8; size];
                reader.read_exact(&mut buf).or_else(Error::io)?;
                Ok(Self::Blob(buf))
            }
            b"commit" => {
                Ok(Self::Commit(GitCommit::read(reader, size)?))
            }
            b"tag" => {
                Ok(Self::Tag(GitTag::read(reader, size).or_else(Error::io)?))
            }
            _ => {
                Err(Error::InvalidObjectFormat("unrecognized header"))
            }
        };
    }
    pub fn write(&self, writer: &mut dyn io::Write) -> Result<(), io::Error> {
        writer.write_all(self.name())?;
        writer.write_all(&[SPACE])?;
        writer.write_all(self.size().to_string().as_bytes())?;
        writer.write_all(&[0])?;
        match self {
            Self::Tree(x) => {
                x.write(writer)?;
            }
            Self::Blob(x) => {
                writer.write_all(x)?;
            }
            Self::Commit(x) => {
                x.write(writer)?;
            }
            Self::Tag(x) => {
                x.write(writer)?;
            }
        };
        return Ok(());
    }

    fn size(&self) -> usize {
        return match self {
            Self::Tree(x) => {
                x.size()
            }
            Self::Blob(x) => {
                x.len()
            }
            Self::Commit(x) => {
                x.size()
            }
            Self::Tag(x) => {
                x.size()
            }
        };
    }

    fn name(&self) -> &[u8] {
        return match self {
            Self::Tree(_) => b"tree",
            Self::Blob(_) => b"blob",
            Self::Commit(_) => b"commit",
            Self::Tag(_) => b"tag",
        };
    }

    pub fn store_unchecked(&mut self, object_store: &mut ObjectStore) -> ObjectId {
        match self {
            GitObject::Tree(tree) => {
                tree.store_unchecked(object_store);
            }
            GitObject::Blob(_) => {}
            GitObject::Commit(_) => {}
            GitObject::Tag(_) => {}
        }
        return object_store.set_object_unchecked(self.clone());
    }
}
