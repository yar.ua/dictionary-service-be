use time::OffsetDateTime;
use crate::database::git::errors::Error;
use crate::database::git::commit::{GitCommit, Person};
use crate::database::git::deferred_object::DeferredGitObject;
use crate::database::git::object::GitObject;
use crate::database::git::object_id::ObjectId;
use crate::database::git::object_store::ObjectStore;
use crate::database::git::tree::{GitTree, GitTreeNode, GitTreeNodeMode};
use std::collections::BTreeMap;


impl DeferredGitObject {
    fn child<'a>(&'a self, object_store: &'a ObjectStore, name: String) -> Result<&'a DeferredGitObject, Error> {
        let tree = self.tree(object_store)?;
        for (_, node) in &tree.nodes {
            if node.name == name {
                return Ok(&node.object);
            }
        }
        return Err(Error::ChildNotFound(name));
    }

    fn get_or_create_child_tree(&mut self, object_store: &ObjectStore, name: String) -> Result<&mut DeferredGitObject, Error> {
        return match self.child(object_store, name.clone()) {
            Ok(object) => {
                if let Self::Loaded(GitObject::Tree(_)) = object {
                    return self.mut_child(object_store, name);
                }
                return Err(Error::ObjectKindMismatch("object is not a tree"));
            }
            Err(Error::ChildNotFound(_)) => self.add_child_tree(object_store, name),
            Err(x) => Err(x),
        };
    }

    fn mut_child(&mut self, object_store: &ObjectStore, name: String) -> Result<&mut DeferredGitObject, Error> {
        let tree = self.mut_tree(object_store)?;
        for (_, node) in &mut tree.nodes {
            if node.name == name {
                return Ok(&mut node.object);
            }
        }
        return Err(Error::ChildNotFound(name));
    }

    fn add_child_tree(&mut self, object_store: &ObjectStore, name: String) -> Result<&mut DeferredGitObject, Error> {
        let tree = self.mut_tree(object_store)?;
        let subtree = GitTree::new();
        let node = GitTreeNode { name: name.clone(), mode: GitTreeNodeMode::Tree, object: DeferredGitObject::Loaded(GitObject::Tree(subtree)) };
        tree.nodes.insert(name.clone(), node);
        return Ok(&mut tree.nodes.get_mut(&name).unwrap().object);
    }

    fn add_child_blob(&mut self, object_store: &ObjectStore, name: String, object: GitObject) -> Result<&DeferredGitObject, Error> {
        let tree = self.mut_tree(object_store)?;
        let node = GitTreeNode { name: name.clone(), mode: GitTreeNodeMode::Blob, object: DeferredGitObject::Loaded(object) };
        tree.nodes.insert(name.clone(), node);
        return Ok(&tree.nodes.get(&name).unwrap().object);
    }

    fn tree<'a>(&'a self, object_store: &'a ObjectStore) -> Result<&'a GitTree, Error> {
        if let GitObject::Tree(tree) = self.loaded(object_store)? {
            return Ok(tree);
        }
        return Err(Error::ObjectKindMismatch("object is not a tree"));
    }

    fn mut_tree(&mut self, object_store: &ObjectStore) -> Result<&mut GitTree, Error> {
        let object = self.load(object_store)?;
        if let GitObject::Tree(tree) = object {
            return Ok(tree);
        }
        return Err(Error::ObjectKindMismatch("object is not a tree"));
    }
}

#[derive(Debug)]
pub struct FullTree<'c> {
    pub(crate) store: &'c mut ObjectStore,
    pub(crate) root: DeferredGitObject,
}

impl<'c> FullTree<'c> {
    pub fn new(store: &'c mut ObjectStore, id: ObjectId) -> Self {
        return Self { store, root: DeferredGitObject::NotLoaded(id) };
    }

    pub fn get_object_at_path(&self, path: &String) -> Result<&GitObject, Error> {
        let components = path.split('/');
        let mut node = &self.root;
        for component in components {
            node = node.child(self.store, component.to_string())?;
        }
        return node.loaded(self.store);
    }

    pub fn remove_object_at_path(&mut self, path: String) -> Result<(), Error> {
        unimplemented!()
    }
    pub fn set_object_at_path(&mut self, path: String, git_object: GitObject) -> Result<&DeferredGitObject, Error> {
        let mut components: Vec<&str> = path.split('/').collect();
        let name = components.pop().ok_or(Error::NotImplemented)?;
        let mut node = &mut self.root;
        for component in components {
            node = node.get_or_create_child_tree(self.store, component.to_string())?;
        }
        return node.add_child_blob(&self.store, name.to_string(), git_object);
    }

    pub fn commit(&mut self) -> ObjectId {
        let tree = self.root.store_unchecked(self.store);
        let person = Person {
            name: "Sashko".to_string(), // TODO: change me
            email: "sashko@yar.org.ua".to_string(), // TODO: change me
            date: OffsetDateTime::now_utc(),
        };
        let commit = GitCommit {
            tree: tree.clone(),
            parents: vec![],
            committer: person.clone(),
            author: person,
            message: "auto".to_string(), // TODO: change me
        };
        return DeferredGitObject::Loaded(GitObject::Commit(commit)).store_unchecked(self.store).clone();
    }

    pub fn three_way_merge(&mut self, left: &FullTree, right: &FullTree) -> Result<ObjectId, Box<dyn std::error::Error>> {
        // if let (Some(root), Some(lroot), Some(rroot)) = self.root.mut_tree(self.store), left.root.tree()
        // return self.root.three_way_merge(self.store, left, right);
        todo!()
    }
}

impl<'c> FullTree<'c> {
    fn iter(&self) -> FullTreeIterator {
        return FullTreeIterator {};
    }
}

struct FullTreeIterator {
    // it: <BTreeMap<std::string::String, GitTreeNode> as bitflags::traits::BitFlags>::Iter,
}

impl Iterator for FullTreeIterator {
    type Item = (String, GitTreeNode);
    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}