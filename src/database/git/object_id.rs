use std::fmt::{Display, Formatter};
use sha1::{Digest, Sha1};
use std::io;
use crate::database::git::object::GitObject;

pub const OBJECT_ID_LEN: usize = 20; // Binary length. ASCII length is twice that size

#[derive(PartialEq, Debug, Clone, Eq, Hash)]
pub struct ObjectId {
    // Hash function can be SHA256 https://git-scm.com/docs/hash-function-transition
    pub value: sha1::digest::Output<Sha1>,
}

impl ObjectId {
    pub fn zero() -> Self {
        return Self::from([0u8; OBJECT_ID_LEN]);
    }

    pub fn from(x: [u8; OBJECT_ID_LEN]) -> Self {
        return Self { value: sha1::digest::Output::<Sha1>::from(x) };
    }

    pub fn read_ascii(reader: &mut dyn io::BufRead) -> Result<Self, io::Error> {
        let mut object_id_ascii: [u8; OBJECT_ID_LEN * 2] = [0; OBJECT_ID_LEN * 2];
        reader.read_exact(&mut object_id_ascii)?;
        let mut object_id: [u8; OBJECT_ID_LEN] = [0; OBJECT_ID_LEN];
        for i in 0..OBJECT_ID_LEN {
            let ascii_idx = i * 2;
            let byte = std::str::from_utf8(&object_id_ascii[ascii_idx..ascii_idx + 2]).unwrap();
            object_id[i] = u8::from_str_radix(byte, 16).unwrap();
        }
        return Ok(Self::from(object_id));
    }

    pub fn read_bytes(reader: &mut dyn io::BufRead) -> Result<Self, io::Error> {
        let mut object_id: [u8; OBJECT_ID_LEN] = [0; OBJECT_ID_LEN];
        reader.read_exact(&mut object_id)?;
        return Ok(Self::from(object_id));
    }

    pub fn from_object(object: &GitObject) -> Self {
        let mut w = Sha1::new();
        object.write(&mut w).unwrap();
        return Self { value: w.finalize() };
    }

    pub fn size_bytes() -> usize {
        return OBJECT_ID_LEN;
    }

    pub fn write_bytes(&self, writer: &mut dyn io::Write) -> Result<(), io::Error> {
        return writer.write_all(self.value.as_slice());
    }

    pub fn size_ascii() -> usize {
        return OBJECT_ID_LEN * 2;
    }

    pub fn write_ascii(&self, writer: &mut dyn io::Write) -> Result<(), io::Error> {
        for i in (0..OBJECT_ID_LEN) {
            let byte = self.value.as_slice()[i];
            write!(writer, "{:02x}", byte)?;
        }
        return Ok(());
    }
}

impl Display for ObjectId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for i in (0..OBJECT_ID_LEN) {
            let byte = self.value.as_slice()[i];
            write!(f, "{:02x}", byte)?;
        }
        return Ok(());
    }
}
