use std::collections::HashSet;
use std::path::Path;
use std::io;
use log;
use crate::datatypes::{Grammeme, NewParadigm, Paradigm, UpdatedParadigm};
use crate::database::git::full_tree::FullTree;
use crate::database::git::object_id::ObjectId;
use crate::database::git::object_store::ObjectStore;
use crate::database::snapshot::Snapshot;
use std::error::Error;
use crate::database::errors::Error as DbError;
use crate::database::git::deferred_object::DeferredGitObject;
use crate::database::git::object::GitObject;
use crate::database::git::tree::GitTree;
use base64::{engine::general_purpose::URL_SAFE_NO_PAD as Base64, Engine as _};


type Response<T> = Result<(T, Snapshot), Box<dyn Error>>;

impl Paradigm {
    fn path(id: &String) -> String {
        let (dir, file) = id.split_at(1);
        return format!("paradigms/{}/{}.tsv", dir, file);
    }
    fn new_id() -> String {
        let id = uuid::Uuid::new_v4();
        return Base64.encode(id.as_bytes());
    }

    pub fn from_object(object: &GitObject) -> Option<Self> {
        if let GitObject::Blob(data) = object {
            let mut reader = io::Cursor::new(data);
            if let Ok(r) = Self::from_bytes(&mut reader) {
                return Some(r);
            }
        }
        return None;
    }

    pub fn to_object(&self) -> GitObject {
        let mut cursor = io::Cursor::new(Vec::new());
        self.to_bytes(&mut cursor).unwrap();
        return GitObject::Blob(cursor.into_inner());
    }

    fn to_bytes(&self, writer: &mut dyn io::Write) -> Result<(), Box<dyn Error>> {
        let mut csv_writer = csv::Writer::from_writer(writer);
        csv_writer.serialize((&self.text, &self.pos, &self.categories))?;
        let writer = csv_writer.into_inner().unwrap();
        write!(writer, "---\n")?;
        for grammeme in &self.grammemes {
            grammeme.to_bytes(writer)?;
        }
        return Ok(());
    }

    fn from_bytes(reader: &mut dyn io::BufRead) -> io::Result<Self> {
        let result: Self = serde_json::from_reader(reader)?;
        return Ok(result);
    }
}

impl Grammeme {
    fn to_bytes(&self, writer: &mut dyn io::Write) -> Result<(), Box<dyn Error>> {
        let mut csv_writer = csv::Writer::from_writer(writer);
        csv_writer.serialize((&self.id, &self.text, &self.categories))?;
        return Ok(());
    }
}

impl NewParadigm {
    fn to_paradigm(self) -> Paradigm {
        return Paradigm::from_new(Paradigm::new_id(), self);
    }
}

pub struct Database {
    // root: Path,
    store: ObjectStore,
}

impl Database {
    pub fn new() -> Self {
        return Self {
            store: ObjectStore::new(),
        };
    }
    pub fn save(&self, root: &Path, revision: ObjectId) {
        self.store.save(root, revision);
    }
    pub fn get_tree(&mut self, head: &ObjectId) -> Result<FullTree, Box<dyn Error>> {
        let tree = GitObject::Tree(GitTree::new());
        return Ok(FullTree {
            store: &mut self.store,
            root: DeferredGitObject::Loaded(tree),
        });
    }
    pub fn commit_tree(&mut self, head: &ObjectId, tree: FullTree) -> Result<ObjectId, Box<dyn Error>> {
        unimplemented!()
    }

    pub fn get_snapshot(&self, snapshot: Snapshot) -> Result<ObjectId, Box<dyn Error>> {
        return Ok(ObjectId::zero());
    }
}


impl Database {
    pub async fn find_paradigms(&self,
                                head: Snapshot,
                                text: Option<String>,
                                pos: Option<String>,
                                len: Option<i32>,
                                offset: Option<i32>,
    ) -> Response<Vec<Paradigm>> {
        unimplemented!()
    }

    pub async fn get_paradigms(&mut self, head: Snapshot, ids: &Vec<String>) -> Response<Vec<Paradigm>> {
        let change_id = self.get_snapshot(head.clone())?;
        let tree = self.get_tree(&change_id)?;
        let snapshot = head.with_change_id(change_id);
        let mut objects = Vec::new();
        for id in ids {
            let path = Paradigm::path(&id);
            match tree.get_object_at_path(&path) {
                Ok(blob_id) => {
                    objects.push(blob_id)
                }
                Err(x) => {
                    log::trace!("Requested paradigm not found {}: {}", id, x);
                }
            }
        }
        let mut paradigms: Vec<Paradigm> = vec![];
        for object in objects {
            if let GitObject::Blob(content) = object {
                let mut cursor = io::Cursor::new(content);
                let paradigm = Paradigm::from_bytes(&mut cursor)?;
                paradigms.push(paradigm);
            }
        }
        return Ok((paradigms, snapshot));
    }

    pub async fn delete_paradigms(&mut self, head: Snapshot, ids: &Vec<String>) -> Response<Vec<String>> {
        let snapshot = self.get_snapshot(head.clone())?;
        let unique_ids = ids.into_iter().collect::<HashSet<_>>();
        let mut tree = self.get_tree(&snapshot)?;

        let mut remove_errors = Vec::new();
        let mut removed_ids: Vec<String> = vec![];
        let unique_ids_len = unique_ids.len();
        for id in unique_ids {
            let result = tree.remove_object_at_path(Paradigm::path(id));
            match result {
                Ok(_) => {
                    removed_ids.push(id.clone());
                }
                Err(err) => {
                    log::trace!("Failed to remove paradigm {}: {}", id, err);
                    remove_errors.push(err);
                }
            }
        }
        if remove_errors.len() > 0 {
            log::error!("Failed to remove {} out of {} paradigms", unique_ids_len, remove_errors.len());
            return Err(Box::new(DbError::CustomError { name: "DeleteError", desc: "Failed to remove one or more paradigms." }));
        }
        // let next_head = self.commit_tree(&snapshot, tree)?;
        let next_head = snapshot.clone();
        let snapshot = match head {
            Snapshot::Branch { branch, revision: _revision } => { Snapshot::Branch { branch, revision: next_head } }
            Snapshot::BranchHead { branch } => { Snapshot::Branch { branch, revision: next_head } }
        };
        return Ok((removed_ids, snapshot));
    }

    pub fn add_paradigms(&mut self, head: Snapshot, paradigms: Vec<NewParadigm>) -> Response<Vec<Paradigm>> {
        let change_id = self.get_snapshot(head.clone())?;
        let mut tree = self.get_tree(&change_id)?;
        let mut added_paradigms: Vec<Paradigm> = vec![];
        for mew_paradigm in paradigms {
            let paradigm = mew_paradigm.to_paradigm();
            _ = tree.set_object_at_path(Paradigm::path(&paradigm.id), paradigm.to_object());
        }
        let next_head = tree.commit();
        return Ok((added_paradigms, head.with_change_id(next_head.clone())));
    }

    pub async fn update_paradigms(&mut self, head: Snapshot, paradigms: Vec<UpdatedParadigm>) -> Response<Vec<Paradigm>> {
        unimplemented!()
        // let snapshot = self.get_snapshot(head.clone())?;
        // let mut tree = self.get_tree(&snapshot)?;
        // let mut updated_paradigms: Vec<Paradigm> = vec![];
        // for updated_paradigm in paradigms {
        //     let path = Paradigm::path(&updated_paradigm.id);
        //     let blob_id = tree.get_blob(&path)?;
        //     let blob = self.get_blob(blob_id)?;
        //     let mut paradigm = Paradigm::from_blob(blob)?;
        //     paradigm.update(updated_paradigm);
        //     let new_blob_id = self.new_blob(paradigm.to_blob())?;
        //     tree.replace_blob(&path, new_blob_id)?; // TODO: What errors would happen here? What would they mean?
        //     updated_paradigms.push(paradigm);
        // }
        // let next_head = self.commit_tree(&snapshot, tree)?;
        // return Ok((updated_paradigms, head.with_change_id(next_head)));
    }
}
