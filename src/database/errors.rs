use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub enum Error {
    NotImplemented,
    NotAuthorized,
    BranchHeadOutdated,
    ObjectNotFound,
    ObjectTypeMismatch,
    CustomError { name: &'static str, desc: &'static str },
}

impl Error {
    fn name(&self) -> String {
        return match self {
            Error::NotImplemented => { "NotImplemented".to_string() }
            Error::NotAuthorized => { "NotAuthorized".to_string() }
            Error::BranchHeadOutdated => { "BranchHeadOutdated".to_string() }
            Error::ObjectNotFound => { "ObjectNotFound".to_string() }
            Error::ObjectTypeMismatch => { "ObjectTypeMismatch".to_string() }
            Error::CustomError { name, desc } => { format!("CustomError[{}]", name) }
        };
    }

    fn description(&self) -> String {
        return match self {
            Error::NotImplemented => {
                "Requested functionality not implemented".to_string()
            }
            Error::NotAuthorized => {
                "Not authorized to perform the requested operation".to_string()
            }
            Error::BranchHeadOutdated => {
                "The head of the branch have been updated, please rebase your changes on top of latest version".to_string()
            }
            Error::ObjectNotFound => {
                "The requested object not found in the store".to_string()
            }
            Error::ObjectTypeMismatch => {
                "The type of requested object does not match the requested type".to_string()
            }
            Error::CustomError { name, desc } => {
                desc.to_string()
            }
        };
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error: {} - {}", self.name(), self.description())
    }
}

impl std::error::Error for Error {}
