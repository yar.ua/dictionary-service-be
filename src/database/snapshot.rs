use crate::database::git::object_id::ObjectId;

impl Snapshot {
    pub fn with_change_id(&self, revision: ObjectId) -> Self {
        return match self {
            Snapshot::Branch { branch, revision: _revision } => { Snapshot::Branch { branch: branch.clone(), revision: revision } }
            Snapshot::BranchHead { branch } => { Snapshot::Branch { branch: branch.clone(), revision: revision } }
        };
    }
}

#[derive(Clone, Debug)]
pub enum Snapshot {
    BranchHead { branch: String },
    Branch { branch: String, revision: ObjectId },
}
