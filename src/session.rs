use rocket::http::{Cookie, CookieJar, SameSite, Status};
use rocket::request::{Outcome, Request, FromRequest};
use rocket::serde::{Deserialize, Serialize};
use crate::datatypes::{Paradigm, UpdatedParadigm, User};
use rocket::State;
use serde_json::json;
use crate::db_fixture::Database;
use crate::errors::Error;

#[derive(Clone, Serialize, Deserialize)]
pub struct Session {
    pub user: User,
}

pub const SESSION_COOKIE_KEY: &str = "session";


#[rocket::async_trait]
impl<'r> FromRequest<'r> for Session {
    type Error = Error;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Error> {
        let session_json: Option<String> = req.cookies().get_private(SESSION_COOKIE_KEY)
            .and_then(|cookie| cookie.value().parse().ok());

        if None == session_json {
            return Outcome::Error((Status::Unauthorized, Error::Unauthorized));
        }

        match serde_json::from_str::<Session>(session_json.unwrap().as_str()) {
            Ok(session) => Outcome::Success(session),
            _ => Outcome::Error((Status::Unauthorized, Error::BadCookie)),
        }
    }
}

pub fn create_session_cookie<'c>(user: User, secure: bool) -> Cookie<'c> {
    let session = Session { user };
    let mut cookie_builder = Cookie::build((SESSION_COOKIE_KEY, serde_json::to_string(&session).unwrap()))
        .same_site(SameSite::Lax)
        .path("/")
        .http_only(true);

    if secure {
        cookie_builder = cookie_builder.secure(true);
    }
    cookie_builder.build()
}
