#![allow(missing_docs)]

use std::collections::HashMap;
use rocket::{Data, Request, State};
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};

use crate::datatypes::{Grammeme, NewParadigm, Paradigm, UpdatedParadigm};
use crate::errors::Error;

#[derive(Default, Clone)]
pub struct Database {
    pub paradigms: HashMap<String, Paradigm>,
    id: i32,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Database {
    type Error = Error;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Error> {
        return Outcome::Error((Status::Unauthorized, Error::Unauthorized));
        // return Outcome::Success(Session(key));
    }
}


impl Database {
    pub fn lock(&self) -> &mut Database {
        unsafe {
            let s: *const Database = self;
            let ms: *mut Database = s as *mut Database;
            return ms.as_mut().expect("");
        }
    }

    pub fn new() -> Database {
        let mut paradigms = HashMap::new();
        paradigms.insert(
            "1000".to_owned(),
            Paradigm {
                id: "1000".to_string(),
                text: "жо́втий".to_string(),
                pos: "A".to_string(),
                categories: "qaul".to_string(),
                grammemes: vec![
                    Grammeme { id: "0".to_string(), text: "жо́втий".to_string(), categories: "sg;m;nom".to_string() },
                    Grammeme { id: "1".to_string(), text: "жо́втого".to_string(), categories: "sg;m;gen".to_string() },
                    Grammeme { id: "2".to_string(), text: "жо́втому".to_string(), categories: "sg;m;dat".to_string() },
                    Grammeme { id: "3".to_string(), text: "жо́втого".to_string(), categories: "sg;m;acc".to_string() },
                    Grammeme { id: "4".to_string(), text: "жо́втому".to_string(), categories: "sg;m;loc".to_string() },
                    Grammeme { id: "5".to_string(), text: "жо́втім".to_string(), categories: "sg;m;loc".to_string() },
                    Grammeme { id: "6".to_string(), text: "жо́втим".to_string(), categories: "sg;m;inst".to_string() },
                    Grammeme { id: "7".to_string(), text: "жо́втий".to_string(), categories: "sg;m;voc".to_string() },
                ],
            },
        );
        paradigms.insert(
            "1001".to_owned(),
            Paradigm {
                id: "1001".to_string(),
                text: "пи́сар".to_string(),
                pos: "N".to_string(),
                categories: "m;anim".to_string(),
                grammemes: vec![
                    Grammeme { id: "0".to_string(), text: "пи́сар".to_string(), categories: "sg;nom".to_string() },
                    Grammeme { id: "1".to_string(), text: "пи́саря".to_string(), categories: "sg;gen".to_string() },
                    Grammeme { id: "2".to_string(), text: "пи́сарю".to_string(), categories: "sg;dat".to_string() },
                    Grammeme { id: "3".to_string(), text: "пи́саря".to_string(), categories: "sg;acc".to_string() },
                    Grammeme { id: "4".to_string(), text: "пи́сарі".to_string(), categories: "sg;loc".to_string() },
                    Grammeme { id: "6".to_string(), text: "пи́сарем".to_string(), categories: "sg;inst".to_string() },
                    Grammeme { id: "7".to_string(), text: "пи́саре".to_string(), categories: "sg;voc".to_string() },
                ],
            },
        );

        Database {
            paradigms,
            id: 1002,
        }
    }

    fn next_id(&mut self) -> String {
        let result = self.id.to_string();
        self.id += 1;
        result
    }

    pub fn get_paradigm(&self, id: &String) -> Option<Paradigm> {
        if let Some(h) = self.paradigms.get(id) {
            Some(h.clone())
        } else {
            None
        }
    }

    pub fn find_paradigms(
        &self,
        text: Option<String>,
        pos: Option<String>,
        len: Option<i32>,
        offset: Option<i32>,
    ) -> Vec<Paradigm> {
        if let Some(text) = text {
            self.paradigms.values().cloned().collect()
        } else {
            self.paradigms.values().cloned().collect()
        }
    }
    pub fn get_paradigms(&self, ids: &Vec<String>) -> Vec<Option<Paradigm>> {
        ids.iter().map(|id| self.get_paradigm(id)).collect()
    }

    pub fn add_paradigm(&mut self, paradigm: &NewParadigm) -> Option<Paradigm> {
        let id = self.next_id();
        self.paradigms
            .insert(id.clone(), Paradigm::from_new(id.clone(), paradigm.clone()));
        self.get_paradigm(&id)
    }

    pub fn add_paradigms(&mut self, paradigms: &Vec<NewParadigm>) -> Vec<Option<Paradigm>> {
        paradigms.iter().map(|p| self.add_paradigm(p)).collect()
    }

    pub fn update_paradigm(&mut self, paradigm: &UpdatedParadigm) -> Option<Paradigm> {
        if let Some(p) = self.paradigms.get_mut(&paradigm.id) {
            p.update(paradigm.clone());
            return Some(p.clone());
        } else {
            return None;
        }
    }
    pub fn update_paradigms(&mut self, paradigms: &Vec<UpdatedParadigm>) -> Vec<Option<Paradigm>> {
        paradigms.iter().map(|p| self.update_paradigm(p)).collect()
    }

    pub fn delete_paradigm(&mut self, id: &String) -> Option<String> {
        if let Some(_) = self.paradigms.remove(id) {
            return Some(id.clone());
        } else {
            return None;
        }
    }
    pub fn delete_paradigms(&mut self, ids: &Vec<String>) -> Vec<Option<String>> {
        ids.iter().map(|id| self.delete_paradigm(id)).collect()
    }
}
