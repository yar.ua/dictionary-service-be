use serde::{Deserialize};

#[derive(Clone, Deserialize)]
pub struct NewGrammeme {
    pub text: String,
    pub categories: String,
}
