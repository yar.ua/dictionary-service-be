use serde::{Deserialize};
use crate::datatypes::grammeme_new::NewGrammeme;

#[derive(Clone, Deserialize)]
pub struct NewParadigm {
    pub pos: String,
    pub text: String,
    pub categories: String,
    pub grammemes: Vec<NewGrammeme>,
}
