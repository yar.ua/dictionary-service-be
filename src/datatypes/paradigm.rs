use crate::datatypes::{Grammeme, NewParadigm, UpdatedParadigm};
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Paradigm {
    pub id: String,
    pub pos: String,
    pub text: String,
    pub categories: String,
    pub grammemes: Vec<Grammeme>,
}

impl Paradigm {
    pub fn from_new(id: String, p: NewParadigm) -> Self {
        Self {
            id,
            pos: p.pos,
            text: p.text,
            categories: p.categories,
            grammemes: p
                .grammemes
                .into_iter()
                .enumerate()
                .map(|(i, g)| Grammeme::from_new(i.to_string(), g))
                .collect(),
        }
    }
    pub fn update(&mut self, update: UpdatedParadigm) {
        if let Some(pos) = update.pos {
            self.pos = pos;
        }
        if let Some(text) = update.text {
            self.text = text;
        }
        if let Some(categories) = update.categories {
            self.categories = categories;
        }
        if let Some(removed) = update.removed_grammemes {
            self.grammemes.retain(|g| !removed.contains(&g.id));
        }
        if let Some(added) = update.new_grammemes {
            let mut grammemes: Vec<Grammeme> = added
                .into_iter()
                .enumerate()
                .map(|(i, g)| Grammeme::from_new(i.to_string(), g))
                .collect();
            self.grammemes.append(&mut grammemes);
        }
        if let Some(grammemes) = update.grammemes {
            self.grammemes
                .iter_mut()
                .for_each(|g| g.try_update_from(&grammemes))
        }
    }
}
