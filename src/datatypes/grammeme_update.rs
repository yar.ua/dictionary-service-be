use serde::{Deserialize};

#[derive(Clone, Deserialize)]
pub struct UpdatedGrammeme {
    pub id: String,
    pub text: Option<String>,
    pub categories: Option<String>,
}
