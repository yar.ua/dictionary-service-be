use serde::{Deserialize, Serialize};
use crate::datatypes::{UpdatedGrammeme, NewGrammeme};

#[derive(Clone, Serialize, Deserialize)]
pub struct Grammeme {
    pub id: String,
    pub text: String,
    pub categories: String,
}

impl Grammeme {
    pub fn from_new(id: String, g: NewGrammeme) -> Self {
        Self {
            id,
            text: g.text,
            categories: g.categories,
        }
    }

    pub fn try_update_from(&mut self, updates: &Vec<UpdatedGrammeme>) {
        for update in updates {
            if update.id == self.id {
                let update = update.clone();
                if let Some(text) = update.text {
                    self.text = text;
                }
                if let Some(categories) = update.categories {
                    self.categories = categories;
                }
                break;
            }
        }
    }
}
