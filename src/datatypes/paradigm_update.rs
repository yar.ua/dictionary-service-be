use serde::{Deserialize};

use crate::datatypes::grammeme_update::UpdatedGrammeme;
use crate::datatypes::grammeme_new::NewGrammeme;

#[derive(Clone, Deserialize)]
pub struct UpdatedParadigm {
    pub id: String,
    pub pos: Option<String>,
    pub text: Option<String>,
    pub categories: Option<String>,
    pub grammemes: Option<Vec<UpdatedGrammeme>>,
    pub removed_grammemes: Option<Vec<String>>,
    pub new_grammemes: Option<Vec<NewGrammeme>>,
}
