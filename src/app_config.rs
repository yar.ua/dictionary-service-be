use rocket::figment::{Figment, Profile};
use rocket::figment::providers::{Toml, Format, Env};
use serde::{Deserialize, Serialize};


#[derive(Debug, Deserialize, Serialize)]
pub struct ConfigOauthGoogle {
    pub provider: String,
    pub client_id: String,
    pub client_secret: String,
    pub redirect_uri: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ConfigOauth {
    pub google: ConfigOauthGoogle,
    pub secure: bool,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    pub port: u16,
    pub cors: bool,
    pub oauth: ConfigOauth,
}

pub fn app_figment() -> Figment {
    Figment::from(rocket::Config::default())
        .merge(Toml::file("Rocket.toml").nested())
        .merge(Toml::file("Yar.Rocket.toml").nested())
        .merge(Env::prefixed("ROCKET_").ignore(&["PROFILE"]).global())
        .select(Profile::from_env_or("ROCKET_PROFILE", rocket::config::Config::DEFAULT_PROFILE))
}
