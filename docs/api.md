# API End Points

## 1 JSON API

All responses are in json format. All responses contain `Content-Type` header set to `application/json`
and `Content-Length` header.

### 1.1 Authentication and User

#### Get user information

##### Path: `GET /api/user`

Get information about the current user.

_NOTE_: Requires an active session. Otherwise, responds with status 401: Unauthorized.

Response type:

```typescript
class Body {
  id: String
  name: String
  email: String
}
```

### 1.2 Branches

#### `GET /api/branches`

Get all available branches. This typically includes all branches created by the user, all release branches and `main`
branch.

### 1.3 Data

#### `GET /api/paradigm/<id:string>`

Parameters:

- `branch` name of the branch. Defaults to `main`. When provided will use the current head of this branch to determine
  the revision.
- `revision` revision from which to take the data. When provided the `branch` is ignored.

Response type:

```typescript
class Body {
  id: String
  name: String
  email: String
}
```
