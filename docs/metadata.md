# Storing and Retrieving Metadata

Each paradigm contains metadata which includes the license under which the content is distributed and the author of this
content. This data is stored in a commit message of the commit that introduced this file.